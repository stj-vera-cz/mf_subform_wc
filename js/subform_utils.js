
function checkValidity(inputId) {
    var inpObj = document.getElementById(inputId);
    if (!inpObj.checkValidity()) {
        return {status:1, errmsg: inpObj.validationMessage};
    } else {
        return {status:0};
    }
}
